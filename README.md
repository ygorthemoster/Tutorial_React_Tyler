# React Tutorial Pt 1: A Comprehensive Guide to Building Apps with React.js and  React Tutorial 1.5: Utilizing Webpack and Babel to build a React.js App
This is an implementation of two tutorials by Tyler, both are freely avaliable at: https://tylermcginnis.com/reactjs-tutorial-a-comprehensive-guide-to-building-apps-with-react/ and https://tylermcginnis.com/react-js-tutorial-1-5-utilizing-webpack-and-babel-to-build-a-react-js-app/, they teach how to create an react app and use webpack to build it, this code is the final product of said tutorial plus a little bit of modifications to run with newwer versions of react, node and npm

## Pre-requisites
To test this project you'll need:
- [git](https://git-scm.com/)
- npm available when installing [Node.js](https://nodejs.org/en/)

## Installing

The install process can be done by typing the following on a terminal:

```
git clone https://ygorthemoster@gitlab.com/ygorthemoster/Tutorial_React_Tyler.git
cd Tutorial_React_Tyler
npm install
webpack
```

## Running
you'll find the built index.html inside the build folder

## License
See [LICENSE](LICENSE)

## Acknowledgments
- Original source and tutorial by [Tyler McGinnis](https://developers.google.com/experts/people/tyler-mcginnis) available at: https://tylermcginnis.com/reactjs-tutorial-a-comprehensive-guide-to-building-apps-with-react/
