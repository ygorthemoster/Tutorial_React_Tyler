import React from 'react';
import PropTypes from 'prop-types';

class ShowList extends React.Component{

  render(){
    var listItems = this.props.names.map(function(friend, key){
      return <li key={key}> {friend} </li>;
    });
    return (
      <div>
        <h3> Friends </h3>
        <ul>
          {listItems}
        </ul>
      </div>
    )
  }
};

ShowList.defaultProps = {
  names: []
}

class AddFriend extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      newFriend: ''
    };
  }

  updateNewFriend(e){
    this.setState({
      newFriend: e.target.value
    });
  }

  handleAddNew(){
    this.props.addNew(this.state.newFriend);
    this.setState({
      newFriend: ''
    });
  }

  render(){
    return (
      <div>
        <input type="text" value={this.state.newFriend} onChange={this.updateNewFriend} />
        <button onClick={this.handleAddNew}> Add Friend </button>
      </div>
    );
  }
};

AddFriend.propTypes = {
  addNew: PropTypes.func.isRequired
}

export default class FriendsContainer extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      name: 'Tyler McGinnis',
      friends: ['Jake Lingwall', 'Murphy Randall', 'Merrick Christensen']
    };
  }

  componentWillMount(){ alert('In Component Will Mount'); }
  componentWillUnmount(){ alert('In Component Will Unmount'); }
  componentDidMount(){ alert('In Component Did Mount'); }
  componentWillReceiveProps(nextProps){alert('In Component Will Receive Props');}

  addFriend(friend){
    this.setState({
      friends: this.state.friends.concat([friend])
    });
  }

  render(){
    return (
      <div>
        <h3> Name: {this.state.name} </h3>
        <AddFriend addNew={this.addFriend} />
        <ShowList names={this.state.friends} />
      </div>
    )
  }
};
