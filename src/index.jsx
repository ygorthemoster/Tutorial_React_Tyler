import React from 'react';
import ReactDOM from 'react-dom';

import HelloWorld from './hello.jsx';
import FriendsContainer from './friends.jsx';

ReactDOM.render(
  (<div>
    <HelloWorld name="Tyler McGinnis" />
    <FriendsContainer />
  </div>),  document.getElementById('app'));
